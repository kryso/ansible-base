FROM python:3-alpine
RUN apk add --no-cache alpine-sdk libffi-dev openssl-dev
RUN pip3 install ansible jmespath ansible-review

COPY ansible.cfg /code/
WORKDIR /code
RUN chmod -R o=rwx /code
CMD ansible-playbook -i inventory/docker/ playbooks/all.yml
